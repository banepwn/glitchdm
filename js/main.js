var selectedSession = lightdm.sessions[0];
var selectedSessionIndex = 0;

/* Listeners required by Lightdm */
function authentication_complete() {
	if (lightdm.is_authenticated) {
		console.log("User is authenticated. Session: " + selectedSession.name);
		lightdm.login(lightdm.authentication_user, selectedSession.key);
	} else {
		console.log("authentication failure");
		document.getElementById("error").style.display = "inline-block";
		document.getElementById("username").disabled = false;
		document.getElementById("password").disabled = false;
	}
}

function show_error(message) {
	console.log("error: " + message);
	alert(message);
}

function show_prompt(message) {
	console.log("prompt: " + message);
	lightdm.provide_secret(document.getElementById("password").value);
}

/* Starts the sign in process */
function submitPassword() {
	console.log("submitPassword");
	document.getElementById("error").style.display = "none";
	document.getElementById("username").disabled = true;
	document.getElementById("password").disabled = true;
	//lightdm.cancel_authentication();
	lightdm.cancel_timed_login();
	lightdm.start_authentication(document.getElementById("username").value);
}

/* Cycle among different sessions */
function cycleSessions(i) {
	selectedSessionIndex += i || 1;
	if (selectedSessionIndex < 0) {
		selectedSessionIndex = lightdm.sessions.length-1;
	} else if (selectedSessionIndex >= lightdm.sessions.length) {
		selectedSessionIndex = 0;
	};
	selectedSession = lightdm.sessions[selectedSessionIndex];
	updateSessionNameContainer();
	document.getElementById("sessionDisplay").style.display = "inline-block";
	console.log("selectedSession: " + selectedSession.name);
}

var userIndex = 0;
function cycleUsers() {
	console.log("cycleUsers");
	document.getElementById("username").value = lightdm.users[++userIndex % lightdm.users.length].name;
}

function updateSessionNameContainer() {
	console.log("updateSessionNameContainer: " + selectedSession.name);
	document.getElementById("sessionNameContainer").innerHTML = selectedSession.name;
}

window.onload = function() {
	document.querySelector("img").onmousedown = function(event) {
		event.preventDefault();
	};
	
	document.querySelectorAll("input").forEach((el) => {
		el.onkeydown = function() {
			if (event.which == 13 || event.which == 10) {
				event.preventDefault();
				submitPassword();
			}
		};
	});

	document.onkeydown = function() {
		if (!event.shiftKey && !event.ctrlKey && event.altKey && !event.metaKey) {
			switch (event.key) {
				case "s": /* Alt + S */
					cycleSessions();
					break;
				case "c": /* Alt + C */
					cycleSessions(-1);
					break;
				case "h": /* Alt + H */
					lightdm.hibernate();
					break;
				case "p": /* Alt + P */
					lightdm.suspend();
					break;
				case "r": /* Alt + R */
					lightdm.restart();
					break;
				case "l": /* Alt + L */
					cycleUsers();
					break;
				case "d": /* Alt + D */
					lightdm.shutdown();
					break;
			}
		} else if (event.key == "Alt") {
			document.getElementById("shortcutsGuide").style.opacity = 1;
		}
	};
	
	document.onkeyup = function() {
		if (event.key == "Alt") {
			document.getElementById("shortcutsGuide").style.opacity = 0;
		}
	};
	
	var defaultUserName;
	lightdm.users.forEach(user => {
		if (user.logged_in) {
			defaultUserName = user.name;
			return;
		}
	});
	document.getElementById("username").value = defaultUserName || lightdm.users[0].name;
	
	document.getElementById("glitch").dataset.text = document.getElementById("glitch").innerText;
	
	updateSessionNameContainer();

	document.getElementById("password").focus();
	
	setTimeout(function() {
		document.getElementById("content").style.opacity = 1;
	}, 250);
	
	// Screensaver
	var blanker = document.getElementById("blanker");
	var sleeping = false;
	var secondsSinceLastActivity = 0;
	function registerActivity() {
		secondsSinceLastActivity = 0;
		if (sleeping) {
			console.log("Waking up!");
			sleeping = false;
			blanker.style.transition = "opacity 0.125s";
			blanker.style.opacity = 0;
		};
	};
	["mousedown", "mousemove", "keydown", "scroll", "touchstart"].forEach(eventName => {
		document.addEventListener(eventName, registerActivity, true);
	});
	setInterval(function() {
		secondsSinceLastActivity++;
		if (!sleeping && secondsSinceLastActivity > 60) {
			sleeping = true;
			console.log("Going to sleep...");
			blanker.style.transition = "opacity 10s";
			blanker.style.opacity = 1;
		};
	}, 1000);
};
