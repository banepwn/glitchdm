# GlitchDM

*"A LightDM Web Greeter theme with an unknown purpose. Extremely rare."* Forked from [bryanbecker/glitchdm](https://github.com/bryanbecker/glitchdm), which in turn was forked from [pedropenna/musfealle](https://github.com/pedropenna/musfealle), which in turn was based on [Antergos/lightdm-webkit-theme-antergos](https://github.com/Antergos/lightdm-webkit-theme-antergos). My fork removes the jQuery and Bootstrap dependencies, cleans up the code, and makes some adjustments based on my own personal preference.

Images in `img/sddm-lain-wired-theme` are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](https://gitlab.com/mixedCase/sddm-lain-wired-theme/raw/master/LICENSE) and are sourced from [Andrés Rodríguez's repository](https://gitlab.com/mixedCase/sddm-lain-wired-theme/tree/master). However, I am a little skeptical that mixedCase was authorized to license these images... Also, this license might be incompatible with the GPL.

## Installation

1. Install [lightdm-webkit2-greeter](https://github.com/Antergos/lightdm-webkit2-greeter).
2. Set lightdm-webkit2-greeter to be the default greeter. The preferred way is to create a file inside `/etc/lightdm/lightdm.conf.d`. This file can have any name you want, but something like `50-lightdm-webkit2-greeter.conf` is not a bad choice. The contents of the file should be:
```
[Seat:*]
greeter-session=lightdm-webkit2-greeter
```
3. `git clone https://banepwn@bitbucket.org/banepwn/glitchdm.git`
4. `sudo cp -r glitchdm /usr/share/lightdm-webkit/themes`
5. Edit the file `/etc/lightdm/lightdm-webkit2-greeter.conf` and set the `webkit-theme` property to `glitchdm`.
6. Reboot.

**Note: If you are using Ubuntu 17.10 or later, your OS is using GDM and not LightDM by default.** Here are the instructions for rectifying that.

1. `sudo apt install lightdm`
2. If the configuration screen does not appear or you already have LightDM installed, `sudo dpkg-reconfigure lightdm`
3. Select `lightdm` from the configuration screen.
4. Reboot.

## Testing

**`lightdm --test-mode` does not work.** Use `lightdm-webkit2-greeter`. You may have to set `X-LightDM-Allow-Greeter=true` in `/usr/share/xsessions/ubuntu.desktop`. If you want a developer console, you could try setting `debug_mode = true` in `/etc/lightdm/lightdm-webkit2-greeter.conf` (untested), or just open the HTML file in GNOME Web since it uses the same engine.

1. `sudo rm -r /usr/share/lightdm-webkit/themes/glitchdm`
2. `sudo cp -r glitchdm /usr/share/lightdm-webkit/themes`

## Keyboard Shortcuts

- Alt + R: Restart
- Alt + D: Shutdown
- Alt + H: Hibernate
- Alt + P: Suspend
- Alt + S or Alt + C: Cycle through the session options
